# README #



### What is this repository for? ###

To narzędzie umożliwia naprawę błędu: "Nie można rzutować obiektu modelu COM typu 'System.__ComObject' na typ interfejsu 'InsERT.Subiekt'" występującego po pobraniu aktualizacji systemu Windows.

Poprawka polega na przywróceniu zawartości plików: 

* C:\Program Files (x86)\Common Files\System\ado\msado60_Backcompat_i386.tlb
* C:\Windows\assembly\GAC\ADODB\7.0.3300.0__b03f5f7f11d50a3a\adodb.dll


# LICENCJA #

SferaFix udostępniamy na licencji MIT.

> Licencja MIT (Licencja X11) – jedna z najprostszych i najbardziej liberalnych licencji otwartego oprogramowania. Daje użytkownikom nieograniczone prawo do używania, kopiowania, modyfikowania i rozpowszechniania (w tym sprzedaży) oryginalnego lub zmodyfikowanego programu w postaci binarnej lub źródłowej. Jedynym wymaganiem jest, by we wszystkich wersjach zachowano warunki licencyjne i informacje o autorze. Podobna do zmodyfikowanej licencji BSD (bez klauzuli ogłoszeniowej) i ISC. Według statystyk serwisu GitHub, licencja MIT jest najczęściej używaną licencją wśród oprogramowania utrzymywanego w tym serwisie[1].

Copyright 2020, RTNET

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
