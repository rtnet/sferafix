﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;


namespace SferaFix
{
    public partial class MainForm : Form
    {
        Dictionary<string, byte[]> destinations = new Dictionary<string, byte[]>() {
            { Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Windows), "assembly\\GAC\\ADODB\\7.0.3300.0__b03f5f7f11d50a3a\\adodb.dll"), Properties.Resources.adodb },
            { Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFilesX86), "System\\ado\\msado60_Backcompat_i386.tlb"), Properties.Resources.msado60_Backcompat_i386 },
        };

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (!Helpers.IsAdministrator())
            {
                var result = MessageBox.Show(this, "Aplikacja wymaga uprawnień administracyjnych." +
                    "\r\nKliknij 'OK' aby uruchomić aplikację z uprawnieniami administracyjnymi", this.Text, MessageBoxButtons.OKCancel);
                if (result == DialogResult.OK)
                {
                    Helpers.ElevateApp();
                }
            }
        }

        private void buttonApply_Click(object sender, EventArgs ea)
        {
            MessageBox.Show(this, "Proszę TERAZ zamknąć wszystkie procesy które mogą korzystać z ADODB (Insert GT, rozwiązania sferyczne).");

            try
            {
                var results = ApplyPatches();
                MessageBox.Show(this, "Operacja zakończona bez błędów.\r\n" + results);
            }
            catch (Exception e)
            {
                MessageBox.Show(this, e.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string ApplyPatches()
        {
            var results = new List<string>();
            foreach (var destination in destinations)
            {
                var result = ApplyPatch(destination.Key, destination.Value);
                results.Add(result);
            }

            return string.Join("\r\n", results);
        }

        private string ApplyPatch(string destination, byte[] data)
        {
            EnsureTargetDirectoryExists(destination);

            if (File.Exists(destination))
            {
                using (var md5 = MD5.Create())
                {
                    var this_hash = md5.ComputeHash(data);
                    var dest_hash = md5.ComputeHash(File.ReadAllBytes(destination));

                    if (this_hash.SequenceEqual(dest_hash))
                    {
                        return $"Łatka nie jest wymagana dla '{destination}'.";
                    }
                }

                string backup_destination = GetBackupFilePath(destination);
                if (File.Exists(backup_destination))
                {
                    string backup_destination2 = backup_destination + "-" + Guid.NewGuid().ToString();
                    File.Move(backup_destination, backup_destination2);
                    File.SetLastWriteTime(backup_destination2, DateTime.Now);
                }

                File.Copy(destination, backup_destination);
            }

            File.WriteAllBytes(destination, data);

            return $"Zastosowano dla pliku '{destination}'.";
        }

        private void EnsureTargetDirectoryExists(string destination)
        {
            string dir_name = Path.GetDirectoryName(destination);
            if (!Directory.Exists(dir_name))
            {
                try
                {
                    Directory.CreateDirectory(dir_name);
                }
                catch (Exception e)
                {
                    throw new Exception($"Nie udało się utworzyć katalogu '{dir_name}': {e.Message}", e);
                }
            }
        }

        private string GetBackupFilePath(string destination)
        {
            return destination + ".BAK";
        }

        private void buttonRemove_Click(object sender, EventArgs ea)
        {
            MessageBox.Show(this, "Proszę TERAZ zamknąć wszystkie procesy które mogą korzystać z ADODB (Insert GT, rozwiązania sferyczne).");

            try
            {
                var results = RemovePatches();
                MessageBox.Show(this, "Operacja zakończona bez błędów.\r\n" + results);
            }
            catch (Exception e)
            {
                MessageBox.Show(this, e.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string RemovePatches()
        {
            var results = new List<string>();
            foreach (var destination in destinations)
            {
                var result = RemovePatch(destination.Key);
                results.Add(result);
            }

            return string.Join("\r\n", results);
        }

        private string RemovePatch(string destination)
        {
            var backup_path = GetBackupFilePath(destination);
            var temp_path = Path.Combine(Path.GetTempPath(), $"{this.Text}-{Guid.NewGuid()}");
            if (!File.Exists(backup_path))
            {
                return $"Brak kopii zapasowej pliku '{destination}'.";
            }

            File.Move(destination, temp_path);
            try
            {
                File.Move(backup_path, destination);
            }
            catch (Exception e)
            {
                File.Move(temp_path, destination);
                throw e;
            }

            return $"Oryginalny plik '{destination}' został przywrócony.";
        }
    }
}
