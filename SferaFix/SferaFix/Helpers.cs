﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Principal;
using System.Diagnostics;
using System.Windows.Forms;

namespace SferaFix
{
    class Helpers
    {
        public static bool IsAdministrator()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        public static void ElevateApp()
        {
            RunAsElevated(System.Reflection.Assembly.GetEntryAssembly().Location);
            System.Threading.Thread.Sleep(1000);
            Application.Exit();
            return;
        }

        public static Process RunAsElevated(string path, string args = "", bool UseShellExecute = true)
        {
            ProcessStartInfo elevated = new ProcessStartInfo(path);
            elevated.UseShellExecute = UseShellExecute;
            elevated.Verb = "runas";
            elevated.Arguments = args;
            elevated.RedirectStandardError = !UseShellExecute;
            elevated.RedirectStandardOutput = !UseShellExecute;
            var process = Process.Start(elevated);
            return process;
        }
    }
}
